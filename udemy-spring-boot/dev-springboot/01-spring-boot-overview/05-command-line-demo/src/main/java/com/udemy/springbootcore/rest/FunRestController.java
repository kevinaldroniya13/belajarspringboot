package com.udemy.springbootcore.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FunRestController {
    @GetMapping("/")
    public String sayHello(){
        return "Side To Side";
    }

    @GetMapping("/song")
    public String getDailySong(){
        return "Ariana - Side To Side";
    }

    @GetMapping("/playlist")
    public String getPlaylist(){
        return "Eve - Shinkai";
    }
}
