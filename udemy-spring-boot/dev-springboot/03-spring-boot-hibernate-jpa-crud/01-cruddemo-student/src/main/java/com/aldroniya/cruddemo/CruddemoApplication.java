package com.aldroniya.cruddemo;

import com.aldroniya.cruddemo.dao.StudentDAO;
import com.aldroniya.cruddemo.entity.Student;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class CruddemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CruddemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(StudentDAO studentDAO){
		return runner ->{
			//createStudent(studentDAO);

			//createMultipleStudent(studentDAO);

			//readStudent(studentDAO);

			//queryStudent(studentDAO);

			//queryForStudentsByFirstName(studentDAO);

			//queryStudentsByLastName(studentDAO);

			updateStudent(studentDAO);

			//deleteStudent(studentDAO);
		};
	}

	private void deleteStudent(StudentDAO studentDAO) {
		//retrieving student based on id : primary key
		int StudentId = 3;
		System.out.println("Deleting student id : "+StudentId);
		studentDAO.delete(StudentId);

	}

	private void updateStudent(StudentDAO studentDAO) {
		//Retrieving student based on id : primary key
		int StudentId = 1;
		System.out.println("Getting student with id : "+StudentId);

		//find student with that id = StudentId
		Student myStudent = studentDAO.findById(StudentId);
		System.out.println("Updating student...");

		//change first name to Gojo
		myStudent.setLastName("Satoru");
		studentDAO.update(myStudent);

		//display updated student
		System.out.println("Updated Student : "+myStudent);
	}

	private void queryStudentsByLastName(StudentDAO studentDAO) {
		//get list of students
		List<Student> theStudents = studentDAO.findByLastName("Smith");

		//display list of students
		for (Student tempStudent : theStudents){
			System.out.println(tempStudent);
		}
	}

	private void queryForStudentsByFirstName(StudentDAO studentDAO) {
		//get a list of students
		List<Student> theStudents = studentDAO.findByFirstName("Gojo");

		//display list of students
		for (Student tempStudent : theStudents){
			System.out.println(tempStudent);
		}
	}

	private void queryStudent(StudentDAO studentDAO) {
		//get list of students
		List<Student> theStudents = studentDAO.findAll();

		//display list of student
		for (Student tempStudent:theStudents){
			System.out.println(tempStudent);
		}
	}

	private void readStudent(StudentDAO studentDAO) {
		//create student object
		System.out.println("Create a new student object...");
		Student tempStudent = new Student("Gojo","Satoru","gojo.satoru@jjk.com");

		//save student object
		System.out.println("Saving the student");
		studentDAO.save(tempStudent);

		//display id of the saved student
		int theId = tempStudent.getId();
		System.out.println("Saved student. Generated Id : "+theId);

		//retrieve student based on the id : primary key
		System.out.println("Retrieving student with id : "+theId);
		Student myStudent = studentDAO.findById(theId);

		//display student
		System.out.println("Found the student : "+myStudent);
	}

	private void createMultipleStudent(StudentDAO studentDAO) {
		//create the student object
		System.out.println("Creating 3 student object...");
		Student tempStudent1 = new Student("John","Smith","john@luv2code.com");
		Student tempStudent2 = new Student("Alpha","Smith","alpha@luv2code.com");
		Student tempStudent3 = new Student("Beta","Smith","beta@luv2code.com");


		//save student object
		System.out.println("Saving the student");
		studentDAO.save(tempStudent1);
		studentDAO.save(tempStudent2);
		studentDAO.save(tempStudent3);
	}

	private void createStudent(StudentDAO studentDAO) {
		//create the student object
		System.out.println("Creating new student object...");
		Student tempStudent = new Student("Paul","Doe","paul@luv2code.com");

		//save student object
		System.out.println("Saving the student");
		studentDAO.save(tempStudent);

		//display if of the saved student
		System.out.println("Saved student. Generate id : " + tempStudent.getId());
	}
}
