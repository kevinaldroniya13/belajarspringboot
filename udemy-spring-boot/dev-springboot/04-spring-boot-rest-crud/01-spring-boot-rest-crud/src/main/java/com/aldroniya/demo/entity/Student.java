package com.aldroniya.demo.entity;


public class Student {
    //field
    private String firstName;
    private String lastName;

    //non argument constructor
    public Student(){

    }

    //generate constructor
    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }


    //generate getter and setter
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
