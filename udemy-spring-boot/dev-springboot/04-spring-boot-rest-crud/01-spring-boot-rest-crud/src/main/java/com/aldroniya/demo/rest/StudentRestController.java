package com.aldroniya.demo.rest;

import com.aldroniya.demo.entity.Student;
import jakarta.annotation.PostConstruct;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentRestController {

    private List<Student> theStudents;

    //define @PostConstruct to load student data
    @PostConstruct
    public void loadData(){
        theStudents = new ArrayList<>();

        theStudents.add(new Student("Megumi","Fushiguro"));
        theStudents.add(new Student("Yuji","Itadori"));
        theStudents.add(new Student("Kugisaki","Nobara"));
    }

    //define endpoint for "/student" - return a list of students
    @GetMapping("/student")
    public List<Student> getStudent(){
        return theStudents;
    }

    //define endpoint for "/student/{studentId}" - return student at index (single student)
    @GetMapping("/student/{studentId}")
    public Student getStudent(@PathVariable int studentId){

        //check the students list size
        if((studentId >= theStudents.size()) || (studentId<0)){
            throw new StudentNotFoundException("Student not found - "+studentId);
        }

        return theStudents.get(studentId);
    }



}
