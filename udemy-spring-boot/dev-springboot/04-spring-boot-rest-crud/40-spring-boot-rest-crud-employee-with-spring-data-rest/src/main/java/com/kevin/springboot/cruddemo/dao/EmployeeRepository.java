package com.kevin.springboot.cruddemo.dao;

import com.kevin.springboot.cruddemo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "sorcerers")
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
