package com.aldroniya.springcoredemo.config;

import com.aldroniya.springcoredemo.common.Coach;
import com.aldroniya.springcoredemo.common.SwimCoach;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class sportConfig {
   @Bean("aquatic")
    public Coach swimCoach(){
       return new SwimCoach();
   }
}
