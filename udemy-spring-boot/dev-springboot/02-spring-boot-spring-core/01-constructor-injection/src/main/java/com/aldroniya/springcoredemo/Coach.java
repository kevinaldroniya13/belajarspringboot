package com.aldroniya.springcoredemo;

public interface Coach {
    String getDailyWorkout();
}
