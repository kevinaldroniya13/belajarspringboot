package com.aldroniya.springcoredemo.common;

public interface Coach {
    String getDailyWorkout();
}
